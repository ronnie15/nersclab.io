# PETSc

PETSc, pronounced PET-see (the S is silent), is a suite of data
structures and routines for the scalable (parallel) solution of
scientific applications modeled by partial differential equations. It
supports MPI, and GPUs through CUDA or OpenCL, as well as hybrid
MPI-GPU parallelism. PETSc (sometimes called PETSc/Tao) also contains
the Tao optimization software library.

## Using PETSc

Cray provides PETSc as part of the programming environment.

```
nersc$ module load cray-petsc
```

Current documentation is available through the `intro_petsc` man page:

```
nersc$ module load cray-petsc
nersc$ man intro_petsc
```

## References

* [PETSc Homepage](https://www.mcs.anl.gov/petsc/)
