# Julia

![Julia Logo](julia-logo.png)

Julia is a high-level, high-performance dynamic programming language for
technical computing.  It has syntax that is familiar to users of many other
technical computing environments.  Designed at MIT to tackle large-scale
partial-differential equation simulation and distributed linear algebra, Julia
features a robust ecosystem of tools for
[optimization,](https://www.juliaopt.org/)
[statistics,](https://juliastats.github.io/)
[parallel programming,](https://julia.mit.edu/#parallel) and 
[data visualization.](https://juliaplots.github.io/)
Julia is actively developed by teams
[at MIT](https://julia.mit.edu/) and 
[in industry,](https://juliacomputing.com/) along with 
[hundreds of domain-expert scientists and programmers from around the world](https://github.com/JuliaLang/julia/graphs/contributors).

## Using Julia at NERSC

Julia version `1.4.2` has is available at NERSC by loading the `julia` module:

```
module load julia
```

This also configures the `JULIA_DEPOT_PATH` and `JULIA_LOAD_PATH` to give
precedence to packages installed in the global environment. Packages such as
[MPI.jl](https://juliaparallel.github.io/MPI.jl/stable/), which need to be
configured specifically for NERSC's systems have been installed in the global
environment.

Any packages you install using `Pkg.add` will be installed to the user's local
julia package directory `~/.julia`.

### Using Julia with MPI

The [MPI.jl](https://juliaparallel.github.io/MPI.jl/stable/) package has been
configured to work with `srun`. For convenience, we also include the
`mpiexecjl` wrapper. Therefore an MPI-enabled julia program `prog.jl` can be
excuted on `N` ranks either using:

```
srun -n N julia prog.jl
```

or

```
mpiexecjl -n N julia prog.jl
```

![Gadfly Demo](ResizedImage600338-gadfly-demo.png)
