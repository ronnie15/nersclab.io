# Perlmutter Readiness

!!! warning
    This page is currently under active development. Check
    back soon for more content.

This page contains recommendations for application developers and
users to "hit the ground running" on
[Perlmutter](https://www.nersc.gov/systems/perlmutter/) which unlike
previous NERSC systems features GPUs (NVIDIA A100).

Testing of performance on relevant hardware and compatibility with the
software environment are both important.

## Getting Started with GPUs

### Introduction to GPUs

### Additional Resources

* [Ampere in depth](https://devblogs.nvidia.com/nvidia-ampere-architecture-in-depth/)

### Slides / Video from training events

### Training Events

## Prebuilt applications

## High-level considerations for using GPUs

Unlike CPUs, accelerator program requires additional care with memory
management. Transfers from Host-to-Device and Device-to-Host are
comparatively much slower than the memory bandwidth and care should be
taken to minimize them.

Additionally, Perlmutter GPUs feature [tensor
cores](https://developer.nvidia.com/tensor-cores) which can
significantly accelerate certain applications.

## Programming Models

The choice of programming model depends on multiple factors including:
number of performance critical kernels, source language, existing
programming model, and portability of algorithm. A 20K line C++ code
with 5 main kernels will have different priorities and choices vs a
10M line Fortran code.

NERSC supports a wide variety of programming models on Perlmutter:

### Native CUDA C/C++

While the standards based approach offers high portability maximum
performance can be obtained by specializing for specific hardware.

* [CUDA](https://developer.nvidia.com/cuda-toolkit)
* [CUB](https://nvlabs.github.io/cub/)

!!! tip
	With C++ it is possible to encapsulate such code through
	template specialization in order to preserve portability.

### Directives

NVIDIA compilers are recommended for obtaining good performance with
directive based approaches.

#### OpenMP

#### OpenACC

### C++ language based

#### ISO C++

!!! Example
	* Parallel STL (pSTL) to achieve parallelism in many STL functions
          * [Intel's implementation](https://software.intel.com/content/www/us/en/develop/articles/get-started-with-parallel-stl.html)
          * [NVIDIA/PGI's roadmap for GPU acceleration](https://developer.download.nvidia.com/video/gputechconf/gtc/2019/presentation/s9770-c++17-parallel-algorithms-for-nvidia-gpus-with-pgi-c++.pdf)
	* [Executors](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0443r12.html) for heterogeneous programming and code execution
	* [mdspan](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0009r6.html) for multi-dimensional arrays
          * [Kokkos implementation](https://github.com/kokkos/mdspan)

#### Kokkos

[kokkos](https://github.com/kokkos)

#### SYCL

[SYCL / DPC++](https://github.com/intel/llvm/tree/sycl)

#### HIP

[HIP](https://github.com/ROCm-Developer-Tools/HIP)

#### Raja

#### Thrust

[thrust](https://docs.nvidia.com/cuda/thrust/index.html)

### Fortran

#### General recommendations

* Ensure code compiles with PGI compilers

#### Directives

* (optional for portability) Port OpenACC to OpenMP offload as
  compiler support for OpenMP 5.0+ matures.

#### ISO Fortran

!!! tip
    With `use cutensorEx` PGI compilers will map Fortran intrinsics
    such as `MATMUL`, `RESHAPE`, `TRANSPOSE` AND `SPREAD` to
    [CuTENSOR](https://developer.nvidia.com/cutensor) library
    calls. ([Details](https://www.pgroup.com/resources/docs/20.4/x86/pgi-cuda-interfaces/index.htm#cftensor-cutensorex))

### Julia

### Python

For more information about preparing Python code for Perlmutter
GPUs, please see this [page](../development/languages/python/perlmutter-prep.md).

### MPI

CUDA aware MPI is supported.

## Machine Learning Applications

### Case studies

## Libraries

## IO Considerations

## AMD CPU Considerations

## Tools

## Running Applications

## General recommendations

!!! fail "Write a validation test"
    Performance doesn't matter if you get the wrong answer!

    * https://doi.org/10.1086/342267
    * https://doi.org/10.1109/MCSE.2017.3971169

* Define benchmarks for performance. These should represent the
  science cases you want to run on Perlmutter.
* Use optimized libraries when possible (FFT, BLAS, etc).
* Start with 1 MPI rank per GPU.
* Start with UVM and add explicit data movement control as needed.
* Minimize data movement (Host to device and device to host transfers).
* Avoid device allocations (Use
  a [pool allocator](https://en.wikipedia.org/wiki/Memory_pool))

### Algorithms

The ability for applications to achieve both portability and high
performance across computer architectures remains an open
challenge.

However there are some general trends in current and emerging HPC
hardware: increased thread parallelism; wider vector units; and deep,
complex, memory hierarchies.

In some cases a [performance portable](portability.md) algorithm can
realized by considering generic "wide vectors" which could map to
either GPU SIMT threads or CPU SIMD lanes.

## Case Studies and Examples

Brief highlights of key themes when porting applications to use GPUs.

## References and Events

* P3HPC Workshop at SC
* [2019 DOE Performance Portability](https://doep3meeting2019.lbl.gov)
* https://performanceportability.org
