# Cori Burst Buffer

The 1.8 PB NERSC Burst Buffer is based on Cray
[DataWarp](https://pubs.cray.com/bundle/XC_Series_DataWarp_User_Guide_S-2558_publish_S2558_final/page/Quick_Start_to_Using_DataWarp.html)
that uses flash or SSD (solid-state drive) technology to significantly
increase the I/O performance on Cori for all file sizes and all access
patterns that sits within the High Speed Network (HSN) on
Cori. Accessible only from compute nodes, the Burst Buffer provides
per-job (or short-term) storage for I/O intensive codes.

The peak bandwidth performance is over 1.7 TB/s with each Burst Buffer
node contributing up to 6.5 GB/s. The number of Burst Buffer nodes
depends on the granularity and size of the Burst Buffer
allocation. Performance is also dependent on access pattern, transfer
size and access method (e.g. MPI I/O, shared files).

!!! warning "Check the DataWarp limitations"

    Make sure to understand all limitations of Burst Buffer reported in
    [Performance Tuning](../performance/io/bb/index.md#general-issues),
    to avoid loss of data and waste of precious compute hours.

* [Examples](../jobs/examples/index.md#burst-buffer)
* [Performance Tuning](../performance/io/bb/index.md)
* [Cray Data Warp User Guide](https://pubs.cray.com/bundle/XC_Series_DataWarp_User_Guide_CLE60UP07_S-2558_publish_S2558_final/page/About_XC_Series_DataWarp_User_Guide.html)
